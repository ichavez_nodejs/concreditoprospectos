const express = require('express');
const router = express.Router();
const { iniciarSesion } = require('../../middlewares/Validators/Usuarios');
const UsuariosController = require('../../controllers/Usuarios/UsuariosController');

router.post('/', iniciarSesion, UsuariosController.iniciarSesionWeb);

module.exports = router;