const express = require('express');
const router = express.Router();
const { verifyToken } = require('../../middlewares/Auth/Auth');
const { getPage, byId, updateEstatus } = require('../../middlewares/Validators/Validadores');
const ProspectosController = require('../../controllers/Validadores/ProspectosController');

router.get('/', verifyToken, getPage, ProspectosController.obtenerTodosProspectos);
router.get('/:id', verifyToken, byId, ProspectosController.obtenerProspectoPorId);
router.patch('/', verifyToken, updateEstatus, ProspectosController.actualizarEstatusObservacion);

module.exports = router;