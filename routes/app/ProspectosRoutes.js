const express = require('express');
const router = express.Router();
const { uploadFiles } = require('../../middlewares/Services/Spaces')
const { verifyToken } = require('../../middlewares/Auth/Auth');
const { getPage, byId, registrarProspecto, subirArchiv } = require('../../middlewares/Validators/Prospectos');
const ProspectosController = require('../../controllers/Promotores/ProspectosController');

router.get('/', verifyToken, getPage, ProspectosController.obtenerTodosProspectos);
router.get('/:id', verifyToken, byId, ProspectosController.obtenerProspectoPorId);
router.post('/', verifyToken, registrarProspecto, ProspectosController.registrarProspecto);
router.post('/files', verifyToken, subirArchiv, uploadFiles.array('archivos', 10), ProspectosController.subirArchivos);

module.exports = router;