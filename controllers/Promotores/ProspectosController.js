const { Prospectos, Estado, Documentos } = require('../../models');
const { sequelize } = require('../../models/index');
const { validationResult } = require('express-validator');
const { decodedToken } = require('../../middlewares/Auth/Auth');
const { errResponse } = require('../../middlewares/HandleError/HandleError')




const registrarProspecto = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            await trans.rollback();
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        let msg = '';
        const data = decodedToken(req.header('Authorization'));
        let [ prospecto, created ] = await Prospectos.findOrCreate({
            where: { rfc: req.body.rfc, nombres: req.body.nombres },
            defaults: {
                nombres: req.body.nombres,
                primerApellido: req.body.primerApellido,
                segundoApellido: req.body.segundoApellido,
                calle: req.body.calle,
                numeroInterior: req.body.numeroInterior,
                colonia: req.body.colonia,
                codigoPostal: req.body.codigoPostal,
                telefono: req.body.telefono,
                rfc: req.body.rfc,
                estatus: req.body.estatus,
                observaciones: '',
                idUsuario: data.idUsuario
            },
            raw: true,
            transaction: trans
        });
        if (created) {
            msg = 'Se ha registrado con exito';
            await trans.commit();
            return res.status(200).send({ error: false, message: msg, data: null });
        } else {
            await trans.rollback();
            msg = 'Este prospecto ya está registrado';
            colors = {};
            return res.status(200).send({ error: false, message: msg, data: null });
        }
    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: error });
    }
}

const obtenerTodosProspectos = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        const data = decodedToken(req.header('Authorization'));
        const { page, limit } = req.query;
        const response = await sequelize.query(
            `SELECT p.*,  e.estatus
            FROM Prospectos AS p
            JOIN Users AS u ON (p.idUsuario = u.idUsuario)
            JOIN Estados AS e ON (p.estatus = e.idEstatus)
            WHERE u.idUsuario = :idUsuario ORDER BY p.idProspecto DESC LIMIT :limit;`,
            {
                replacements: { idUsuario: data.idUsuario, limit: [parseInt(page), parseInt(limit)] },
                type: sequelize.QueryTypes.SELECT,
                transaction: trans
            });
        await trans.commit();
        return res.status(200).send({ error: false, message: 'Se ha tenido exito', data: response })
    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: null })
    }
}

const obtenerProspectoPorId = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        const data = decodedToken(req.header('Authorization'));
        const idProspecto = req.params.id;
        let response = await Prospectos.findByPk(idProspecto, {
            attributes: ['nombres', 'primerApellido', 'segundoApellido', 'estatus', 'observaciones'],
            transaction: trans,
            raw: true
        });
        let estatus = await Estado.findByPk(response.estatus, { attributes: ['estatus'], raw: true });
        response.estatus = estatus.estatus;

        await trans.commit();
        return res.status(200).send({ error: false, message: 'Se ha tenido exito', data: response });
    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: null });
    }
}

const subirArchivos = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        let lista = [];
        let obj = {};
        let archivos = req.files !== '' && req.files !== null && req.files !== undefined
            ? req.files : '';

        archivos.forEach(async (element, index, array) => {
            obj.nombreDoc = element.originalname;
            obj.urlDoc = element.location;
            obj.idProspecto = parseInt(req.body.idProspecto);
            lista.push(obj);
            obj = {};
        });
        let response = await Documentos.bulkCreate(lista, {
            transaction: trans,
            raw: true
        });

        await trans.commit();
        return res.status(200).send({ error: false, message: 'Se ha tenido exito', data: response });
    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: null });
    }
}


module.exports = {
    registrarProspecto,
    obtenerProspectoPorId,
    obtenerTodosProspectos,
    subirArchivos
}