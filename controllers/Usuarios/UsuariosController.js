const bcrypt = require('bcrypt');
const { User } = require('../../models');
const { sequelize } = require('../../models/index');
const { validationResult } = require('express-validator');
const { generateToken } = require('../../middlewares/Auth/Auth');
const { errResponse } = require('../../middlewares/HandleError/HandleError')




const iniciarSesionApp = async (req, res) => {
    const trans = await sequelize.transaction();
    try {

        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        const { email, password } = req.body;
        let response = await User.findOne({
            where: { email: email, perfil: 2 },
            attributes: { exclude: ['perfil', 'activo'] },
            transaction: trans,
            raw: true
        });
        if (bcrypt.compareSync(password, response.password)) {
            await trans.commit();
            delete response.password;
            response.token = generateToken(response);
            return res.status(200).send({ error: false, message: 'Bienvenido', data: response });
        } else {
            await trans.rollback();
            return res.status(403).send({ error: true, message: 'Datos de inicio de sesión incorrectos', data: null });
        }

    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: null });
    }
}

const encriptaPassword = async (password) => {
    return bcrypt.hashSync(password, 10)
}

const iniciarSesionWeb = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        const { email, password } = req.body;
        let response = await User.findOne({
            where: { email: email, perfil: 1 },
            attributes: { exclude: ['perfil', 'activo'] },
            transaction: trans,
            raw: true
        });
        if (response && bcrypt.compareSync(password, response.password)) {
            await trans.commit();
            delete response.password;
            response.token = generateToken(response);
            return res.status(200).send({ error: false, message: 'Bienvenido', data: response });
        } else {
            await trans.rollback();
            return res.status(403).send({ error: true, message: 'Datos de inicio de sesión incorrectos', data: null });
        }

    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: null });
    }
}


module.exports = {
    iniciarSesionApp,
    iniciarSesionWeb
}