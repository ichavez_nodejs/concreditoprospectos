const { Prospectos, Estado } = require('../../models');
const { sequelize } = require('../../models/index');
const { validationResult } = require('express-validator');
const { errResponse } = require('../../middlewares/HandleError/HandleError')


const obtenerTodosProspectos = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        const { page, limit } = req.query;
        const response = await sequelize.query(
            `SELECT p.*, e.estatus
            FROM Prospectos AS p
            JOIN Users AS u ON (p.idUsuario = u.idUsuario)
            JOIN Estados AS e ON (p.estatus = e.idEstatus) LIMIT :limit;`,
            {
                replacements: { limit: [parseInt(page), parseInt(limit)] },
                type: sequelize.QueryTypes.SELECT,
                transaction: trans
            });
        await trans.commit();
        return res.status(200).send({ error: false, message: 'Se ha tenido exito', data: response })
    } catch (error) {
        await trans.rollback();
        console.log(error)
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: error })
    }
}

const obtenerProspectoPorId = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        const idProspecto = req.params.id;
        let response = await Prospectos.findByPk(idProspecto, {
            attributes: ['idProspecto','nombres', 'primerApellido', 'segundoApellido', 'estatus', 'observaciones'],
            transaction: trans,
            raw: true
        });
        let estatus = await Estado.findByPk(response.estatus, { attributes: ['estatus'], raw: true });
        response.estatus = estatus.estatus;

        await trans.commit();
        return res.status(200).send({ error: false, message: 'Se ha tenido exito', data: response });
    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: null });
    }
}

const actualizarEstatusObservacion = async (req, res) => {
    const trans = await sequelize.transaction();
    try {
        let err = await errResponse(validationResult(req), res, 'error');
        if (err !== null) {
            return res.status(422).send({ error: true, message: 'Informacion incompleta, favor de validar los siguientes campos', data: err.dataErr });
        }
        const { idProspecto, estatus, observaciones } = req.body;
        let response = await Prospectos.update({ estatus: estatus, observaciones: observaciones }, {
            where: { idProspecto: idProspecto },
            transaction: trans,
            raw: true
        });

        await trans.commit();
        return res.status(200).send({ error: false, message: 'Se ha tenido exito', data: {} });
    } catch (error) {
        await trans.rollback();
        return res.status(500).send({ error: true, message: 'Ha ocurrido un error, contacte al administrador', data: null });
    }
}


module.exports = {
    obtenerProspectoPorId,
    obtenerTodosProspectos,
    actualizarEstatusObservacion
}