require('dotenv').config()

module.exports = {
  development: {
    username: process.env.USERNAME_BD,
    password: process.env.PASSWORD_BD,
    database: process.env.DATABASE_MYSQL,
    host: process.env.HOST_BD,
    port: process.env.PORT_BD,
    dialect: "mysql",
    operatorsAliases: 0,
    define: {
      timestamps: false
    },
    logging: 0
  },
  test: {
    username: process.env.USERNAME_BD,
    password: process.env.PASSWORD_BD,
    database: process.env.DATABASE_MYSQL,
    host: process.env.HOST_BD,
    port: process.env.PORT_BD,
    dialect: "mysql",
    operatorsAliases: 0,
    define: {
      timestamps: false
    },
    logging: 0
  },
  production: {
    username: process.env.USERNAME_BD,
    password: process.env.PASSWORD_BD,
    database: process.env.DATABASE_MYSQL,
    host: process.env.HOST_BD,
    port: process.env.PORT_BD,
    dialect: "mysql",
    operatorsAliases: 0,
    define: {
      timestamps: false
    },
    logging: 0
  }
}
