

const { check } = require('express-validator');
module.exports = {
    iniciarSesion: [
        check('email').isString().withMessage('Se requiere su email de usuario'),
        check('password').isString().withMessage('Se requiere la constraseña'),
    ]
};
