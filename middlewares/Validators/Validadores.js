
const { query, param, check } = require('express-validator');
module.exports = {
    getPage: [
        query('page').isInt().withMessage('Se requiere un paginado'),
        query('limit').isInt().withMessage('Se requiere limite de elementos'),
    ],
    byId: [
        param('id').isInt().withMessage('Se requiere el identificador del elemento'),
    ],
    updateEstatus: [
        check('idProspecto').isInt().withMessage('Se requiere el identificador del prospecto'),
        check('estatus').isInt().withMessage('Se requiere el identificador del estatus'),
        check('observaciones').isString().withMessage('El parametro observaciones es requerido'),
    ],
};
