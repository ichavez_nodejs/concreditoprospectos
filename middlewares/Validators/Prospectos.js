const { query, param, check } = require('express-validator');
module.exports = {
    getPage: [
        query('page').isInt().withMessage('Se requiere un paginado'),
        query('limit').isInt().withMessage('Se requiere limite de elementos'),
    ],
    byId: [
        param('id').isInt().withMessage('Se requiere el identificador del elemento'),
    ],
    registrarProspecto:[
        check('nombres').isString().withMessage('El parametro nombres es requerido'),
        check('primerApellido').isString().withMessage('El parametro primerApellido es requerido'),
        check('calle').isString().withMessage('El parametro calle es requerido'),
        check('codigoPostal').isInt().withMessage('El parametro numeroInterior es requerido'),
        check('colonia').isString().withMessage('El parametro colonia es requerido'),
        check('numeroInterior').isString().withMessage('El parametro codigoPostal es requerido'),
        check('telefono').isString().withMessage('El parametro telefono es requerido'),
        check('rfc').isString().withMessage('El parametro rfc es requerido'),
        check('estatus').isInt().withMessage('El parametro estatus es requerido'),        
    ],
    subirArchiv: [
        check('idProspecto').isInt().withMessage('Se requiere el identificador del prospecto')
    ],
};
