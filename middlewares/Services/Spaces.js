if (process.env.NODE_ENV =='development') {
    require('dotenv').config();
}
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const spacesEndpoint = new aws.Endpoint(process.env.ENDPOINT_SPACES);
const s3 = new aws.S3({
    endpoint: spacesEndpoint
});

//CONFIGURACION DE DIGITALOCEAN-SPACE O AWS-S3
const storageFiles = multerS3({
    s3: s3,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    bucket: 'prospectoscc/docs',
    acl: 'public-read',
    metadata: (req, file, cb) => {
        cb(null, {
            fieldname: file.fieldname
        })
    }
});

const uploadFiles = multer({ storage: storageFiles });

module.exports = {
    uploadFiles
};
