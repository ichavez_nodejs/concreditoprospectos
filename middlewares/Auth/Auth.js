const jwt = require('jsonwebtoken');
require('dotenv')

const generateToken = (data) => {
    try {
        const token = jwt.sign(data, process.env.SECRET_TOKEN, { expiresIn: '30d' });
        return token;
    } catch (error) {
        return null;
    }
}
const decodedToken = (header) => {
    try {
        const decoded = jwt.verify(header.split(' ')[1], process.env.SECRET_TOKEN);
        return decoded;
    } catch (error) {
        return null;
    }
}

const verifyToken = (req, res, next) => {
    try {
        const decoded = jwt.verify(req.header('Authorization').split(' ')[1], process.env.SECRET_TOKEN);
        req = decoded.data;
        next();
    } catch (error) {
        switch (error.name) {
            case 'JsonWebTokenError':
                return res.status(403).send({ error: true, message: 'Token incorrecto' });
            case 'TokenExpiredError':
                return res.status(403).send({ error: true, message: 'Token expirado' });
            default:
                return res.status(403).send({ error: true, message: 'Ocurrio un error con el token' });
        }
    }
}

module.exports = {
    generateToken,
    decodedToken,
    verifyToken
}