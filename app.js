const express = require('express');
const cors = require('cors');
const app = express();


//Ruteadores
const appProspectosRoutes = require('./routes/app/ProspectosRoutes');
const webProspectosRoutes = require('./routes/web/ProspectosRoutes');
const webSessionRoutes = require('./routes/web/SesionRoutes');
const appSessionRoutes = require('./routes/app/SesionRoutes');


if (process.env.NODE_ENV === 'development') {
    /**Importar el modulo dotenv para lectura del archivo .env */
    require('dotenv').config();
    /**Sincronizacion de modelos y BD solo para pruebas */
    //sequelize.sync({alter: true});
}


app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.text());

app.use('/app/prospectos', appProspectosRoutes);
app.use('/web/prospectos', webProspectosRoutes);
app.use('/web/iniciar', webSessionRoutes);
app.use('/app/iniciar', appSessionRoutes);

app.get('*', (req, res) => {
    return res.status(404).send({ error: true, message: 'Not found' });
});

app.listen(process.env.PORT, () => {
    console.log(`Server on port http://localhost:${process.env.PORT}`);
});