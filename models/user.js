'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    idUsuario: {
      type:DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      comment:'Identificador, llave primaria'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
      comment:'Email de usuario'
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
      comment:'contraseña de usuario'
    },
    nombres: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
      comment:'Nombre del evaluador - promotor'
    },
    perfil: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment:'Perfil 1 - Evaluador / 2 - Promotor'
    },
    activo: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment:'Estatus de usuario 1 activo 0 inactivo'
    }
  }, {
    sequelize,
    indexes: [
      { fields: ['idUsuario'], unique: true }
    ],
    modelName: 'User',
  });
  return User;
};