'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Prospectos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Prospectos.init({
    idProspecto: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      comment: "Identificador llave primaria",
      autoIncrement: true
    },
    nombres: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      comment: "Nombre(s) del prospecto",
    },
    primerApellido: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      comment: "primerApellido del prospecto",
    },
    segundoApellido: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: "",
      comment: "segundoApellido del prospecto",
    },
    calle: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      comment: "calle del prospecto",
    },
    numeroInterior: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      comment: "numeroInterior del prospecto",
    },
    colonia: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      comment: "colonia del prospecto",
    },
    codigoPostal: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "codigoPostal del prospecto",
    },
    telefono: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      comment: "telefono del prospecto",
    },
    rfc: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      comment: "rfc del prospecto",
    },
    observaciones: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: "",
      comment: "observaciones del rechazo",
    },
    estatus: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "estatus de validacion",
      references: {
        model: 'Estados',
        key: 'idEstatus'
      }
    },
    idUsuario: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "Llave foranea, para idusuario",
      references: {
        model: 'Users',
        key: 'idUsuario'
      }
    },
  }, {
    sequelize,
    indexes: [
      { fields: ['idProspecto', 'estatus','idUsuario'], unique: true }
    ], modelName: 'Prospectos',
  });
  return Prospectos;
};