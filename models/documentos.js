'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Documentos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

    }
  };
  Documentos.init({
    idDocumentos: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      comment: "Identificador llave primaria",
      autoIncrement: true
    },
    nombreDoc: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: '',
      comment:'Columna para el nombre del archivo en la cloud'
    },
    urlDoc: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: '',
      comment:'Columna para url del archivo en la cloud'
    },
    idProspecto: {
      type:DataTypes.INTEGER,
      allowNull: false,
      comment:'Llave foranea para el Prospecto',
      references:{
        model:'Prospectos',
        key:'idProspecto'
      }
    }
  }, {
    sequelize,
    indexes:[
      { fields: ['idProspecto','idDocumentos'], unique: true }
    ],
    modelName: 'Documentos',
  });
  return Documentos;
};