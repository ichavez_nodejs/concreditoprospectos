'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Estado extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

    }
  };
  Estado.init({
    idEstatus: {
      type:DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
      comment:'Identificador llave primaria'
    },
    estatus: {
      type:DataTypes.STRING,
      defaultValue:'',
      allowNull: false,
      comment:'Nombre del estatus'
    }
  }, {
    sequelize,
    indexes: [
      { fields: ['idEstatus'], unique: true }
    ], 
    modelName: 'Estado',
  });
  return Estado;
};